package userLogin;



import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.Component;
import org.zkoss.zul.Button;
import org.zkoss.zul.Checkbox;
import org.zkoss.zul.Datebox;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Radiogroup;
import org.zkoss.zul.Textbox;

import com.sun.istack.internal.logging.Logger;

public class RegistrationComposer extends SelectorComposer<Component>{
	@Wire
	private Button submitButton;
	@Wire
	private Checkbox acceptTermBox;
	@Wire("#nameBox")
	private Textbox nameBox;
	@Wire("#genderRadio")
	private Radiogroup genderRadio;
	@Wire("#birthdayBox")
	private Datebox birthdayBox;
	
	
	
	@Listen("onCheck = #acceptTermBox")
	public void changeSubmitStatus(){
		if(acceptTermBox.isChecked()){
			submitButton.setDisabled(false);
			submitButton.setImage("image/checked.png");
		}else{
			submitButton.setDisabled(true);
            submitButton.setImage("");
		}		
	}
	
	@Listen("onClick = #resetButton")
	public void reset(){
		nameBox.setRawValue("");
		genderRadio.setSelectedIndex(0);
		birthdayBox.setRawValue(null);
		acceptTermBox.setChecked(false);
		submitButton.setDisabled(true);
		submitButton.setImage("");
	}

	@Listen("onClick = #submitButton")
	public void submit(){
		Messagebox.show("Congradulation: "+nameBox.getValue()+" You are have registerd successfully " );
		
	}
	
	
	
	


	
	
	
}
