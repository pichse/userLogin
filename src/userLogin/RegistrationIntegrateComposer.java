package userLogin;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zk.ui.select.annotation.Listen;

import com.sun.istack.internal.logging.Logger;

public class RegistrationIntegrateComposer extends SelectorComposer<Component>{
	private static Logger logger = Logger.getLogger(RegistrationIntegrateComposer.class.getName(), null);
    private LegacyRegister legacyRegister = new LegacyRegister();
    private User newUser = new User();
 
    @Listen("onClick = #submitButton")
    public void register(){
    	legacyRegister.add(newUser);
        //logger.debug("a user was add");
    }
}
